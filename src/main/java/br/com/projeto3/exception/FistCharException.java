package br.com.projeto3.exception;

public class FistCharException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public FistCharException(){
		super("Character único não encontrado.");
	}

}