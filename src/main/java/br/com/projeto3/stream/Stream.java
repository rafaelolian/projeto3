package br.com.projeto3.stream;

public interface Stream {
	
	char getNext();
	
	boolean hasNext();
	
}