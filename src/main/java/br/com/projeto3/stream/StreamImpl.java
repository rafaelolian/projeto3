package br.com.projeto3.stream;

public class StreamImpl implements Stream {

	private final char[] stream;
	private int index=0;

	public StreamImpl( final char[] stream ){
		this.stream = stream;
	}

	public StreamImpl( final String stream ){
		this.stream = stream.toCharArray();
	}

	public char getNext() {
		return stream[index++];
	}

	public boolean hasNext() {
		return index < stream.length;
	}
}