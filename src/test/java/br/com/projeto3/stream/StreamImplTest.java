package br.com.projeto3.stream;

import org.junit.Assert;
import org.junit.Test;

import br.com.projeto3.stream.Stream;
import br.com.projeto3.stream.StreamImpl;

public class StreamImplTest {

	@Test
	public void deveRetornarOPrimeiroCaracter() {
		final Stream stream = new StreamImpl("ABCD");
		Assert.assertEquals('A', stream.getNext());
	}
	
	@Test
	public void deveRetornarOUltimoCaracter() {
		final Stream stream = new StreamImpl("ABCD");
		stream.getNext();
		stream.getNext();
		stream.getNext();
		Assert.assertEquals('D', stream.getNext());
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void deveRetornarExcecaoQuandoNaoHouverMaisCaracteres(){
		final Stream stream = new StreamImpl("ABCD");
		stream.getNext();
		stream.getNext();
		stream.getNext();
		stream.getNext();
		stream.getNext();
	}
	
	@Test
	public void deveExistirMaisCaracteres(){
		final Stream stream = new StreamImpl("ABCD");
		stream.getNext();
		Assert.assertTrue(stream.hasNext());
	}

	@Test
	public void naoDeveExistirMaisCaracteres(){
		final Stream stream = new StreamImpl("ABCD");
		stream.getNext();
		stream.getNext();
		stream.getNext();
		stream.getNext();
		Assert.assertFalse(stream.hasNext());
	}

}