package br.com.projeto3.reader.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.projeto3.exception.FistCharException;
import br.com.projeto3.stream.Stream;

public class ReaderArrayList {
	
	public static char firstChar( final Stream stream ) throws FistCharException {
		final List<Character> chars = new ArrayList<Character>(); 
		
		while( stream.hasNext() ){
			final char character = stream.getNext();
			
			int index = chars.indexOf(character);
			if( index > -1 ){
				chars.remove(index);
			} else {
				chars.add(character);
			}
		}
		if( chars.isEmpty() ){
			throw new FistCharException();
		}		
		return chars.get(0);
	}
}