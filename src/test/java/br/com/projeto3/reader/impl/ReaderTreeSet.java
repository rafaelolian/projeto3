package br.com.projeto3.reader.impl;

import java.util.Set;
import java.util.TreeSet;

import br.com.projeto3.exception.FistCharException;
import br.com.projeto3.stream.Stream;

public class ReaderTreeSet {
	
	public static char firstChar( final Stream stream ) throws FistCharException {
		final Set<Character> chars = new TreeSet<Character>(); 
		
		while( stream.hasNext() ){
			final char character = stream.getNext();
			
			if( chars.contains(character) ){
				chars.remove(character);
			} else {
				chars.add(character);
			}
		}
		if( chars.isEmpty() ){
			throw new FistCharException();
		}		
		return chars.iterator().next();
	}
}