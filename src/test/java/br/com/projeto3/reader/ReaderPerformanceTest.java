package br.com.projeto3.reader;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

import br.com.projeto3.reader.impl.ReaderArrayList;
import br.com.projeto3.reader.impl.ReaderLinkedHashSet;
import br.com.projeto3.reader.impl.ReaderLinkedList;
import br.com.projeto3.reader.impl.ReaderTreeSet;
import br.com.projeto3.stream.Stream;
import br.com.projeto3.stream.StreamImpl;

public class ReaderPerformanceTest {
	
	@Test
	public void deveAvaliarQualReaderEMaisRapido() {
		final StringBuilder builder = new StringBuilder();
		builder.append( RandomStringUtils.randomAlphabetic(10000000) );
		builder.append( '1' );
		builder.append( RandomStringUtils.randomAlphabetic(10000000) );
		builder.append( '2' );
		builder.append( RandomStringUtils.randomAlphabetic(10000000) );
		
		final Stream stream1 = new StreamImpl(builder.toString());
		final Stream stream2 = new StreamImpl(builder.toString());
		final Stream stream3 = new StreamImpl(builder.toString());
		final Stream stream4 = new StreamImpl(builder.toString());
		
		long start = System.currentTimeMillis();
		char fist = ReaderArrayList.firstChar(stream1);
		Assert.assertEquals('1', fist);
		System.out.println( (System.currentTimeMillis()-start) + " : ReaderArrayList");
		
		start = System.currentTimeMillis();
		fist = ReaderLinkedHashSet.firstChar(stream2);
		Assert.assertEquals('1', fist);
		System.out.println( (System.currentTimeMillis()-start) + " : ReaderLinkedHashSet");
		
		start = System.currentTimeMillis();
		fist = ReaderLinkedList.firstChar(stream3);
		Assert.assertEquals('1', fist);
		System.out.println( (System.currentTimeMillis()-start) + " : ReaderLinkedList");
		
		start = System.currentTimeMillis();
		fist = ReaderTreeSet.firstChar(stream4);
		Assert.assertEquals('1', fist);
		System.out.println( (System.currentTimeMillis()-start) + " : ReaderTreeSet");
	}
}