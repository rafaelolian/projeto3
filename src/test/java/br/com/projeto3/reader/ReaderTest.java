package br.com.projeto3.reader;

import org.junit.Assert;
import org.junit.Test;

import br.com.projeto3.exception.FistCharException;
import br.com.projeto3.reader.impl.ReaderArrayList;
import br.com.projeto3.reader.impl.ReaderLinkedList;
import br.com.projeto3.reader.impl.ReaderTreeSet;
import br.com.projeto3.stream.StreamImpl;

public class ReaderTest {
	
	@Test
	public void deveLocalizarOPrimeiroCaracterReaderArrayList(){
		char firstChar = ReaderArrayList.firstChar(new StreamImpl("aAbBABac"));
		Assert.assertEquals('b', firstChar);
	}

	@Test(expected=FistCharException.class)
	public void deveLancarExcecaoTratadaQuandoNaoHouverCharacterQueNaoSeRepitaReaderArrayList(){
		ReaderArrayList.firstChar(new StreamImpl("aAbBABacbc"));
	}

	@Test
	public void deveLocalizarOPrimeiroCaracterReaderLinkedList(){
		char firstChar = ReaderLinkedList.firstChar(new StreamImpl("aAbBABac"));
		Assert.assertEquals('b', firstChar);
	}

	@Test(expected=FistCharException.class)
	public void deveLancarExcecaoTratadaQuandoNaoHouverCharacterQueNaoSeRepitaReaderLinkedList(){
		ReaderLinkedList.firstChar(new StreamImpl("aAbBABacbc"));
	}
	
	@Test
	public void deveLocalizarOPrimeiroCaracterReaderLinkedHashSet(){
		char firstChar = Reader.firstChar(new StreamImpl("aAbBABac"));
		Assert.assertEquals('b', firstChar);
	}

	@Test(expected=FistCharException.class)
	public void deveLancarExcecaoTratadaQuandoNaoHouverCharacterQueNaoSeRepitaReaderLinkedHashSet(){
		Reader.firstChar(new StreamImpl("aAbBABacbc"));
	}
		
	@Test
	public void deveLocalizarOPrimeiroCaracterReaderTreeSet(){
		char firstChar = ReaderTreeSet.firstChar(new StreamImpl("aAbBABac"));
		Assert.assertEquals('b', firstChar);
	}
	
	@Test(expected=FistCharException.class)
	public void deveLancarExcecaoTratadaQuandoNaoHouverCharacterQueNaoSeRepitaReaderTreeSet(){
		ReaderTreeSet.firstChar(new StreamImpl("aAbBABacbc"));
	}
}