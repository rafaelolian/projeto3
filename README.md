# Projeto3

Desenvolver um leitor de Stream capaz de selecionar o primeiro carácter que não se repete.

## Estratégia
Desenvolver um projeto leve e altamente testável utilizando Java 8 e ferramentas fortemente consolidadas no mercado de desenvolvimento de *software* corporativo.

## Arquitetura

Visando atender a estratégia estabelecida, o projeto foi desenvolvido utilizando as tecnologias:

* Java 8
* Maven
* JUnit
* Apache Commons

Foram implementados diversos métodos de pesquisa de Stream utilizando diferentes implementações da API Java Collection para comparação de performance:

* ReaderArrayList.java
* ReaderLinkedHashSet.java
* ReaderLinkedList.java
* ReaderTreeSet.java

A classe ReaderPerformanceTest foi desenvolvida para apresentar um comparativo de performance entre as implementações de busca, os testes foram realizados utilizando um Stream randômico de 30 milhões de caracteres de entrada, onde os únicos caracteres que não repetem são '1' e '2'.

## Padrões

Foram adotados alguns padrões e boas práticas no desenvolvimento do Projeto2, visando facilita o entendimento e melhorar o design do código fonte:

* TDD - Test Driven Development
* Objeto imutável


## Resultados 
Os testes foram realizados no seguinte ambiente:

* Linux Mint 17
* Java 1.8 HotSpot 64-Bit
* Intel Core i3

Formato da resposta: 
```
#!java

<Tempo em milissegundos> : <Nome da implementação>
```


Primeiro teste:

```
#!java

1946 : ReaderArrayList
1322 : ReaderLinkedHashSet
2667 : ReaderLinkedList
3441 : ReaderTreeSet

```

Segundo teste:

```
#!java
1839 : ReaderArrayList
1134 : ReaderLinkedHashSet
2666 : ReaderLinkedList
3392 : ReaderTreeSet
```

Terceiro teste:

```
#!java
1904 : ReaderArrayList
1134 : ReaderLinkedHashSet
2584 : ReaderLinkedList
3476 : ReaderTreeSet
```

## Conclusão 

A implementação mais rápida verificada nos testes foi a classe ReaderLinkedHashSet, baseada em java.util.LinkedHashSet, levando aproximadamente 1 segundo para processar um Stream de 30 milhões de caracteres, segue abaixo sua implementação:

```
#!java

public static char firstChar( final Stream stream ) throws FistCharException {
	final Set<Character> chars = new LinkedHashSet<Character>(); 
	
	while( stream.hasNext() ){
		final char character = stream.getNext();
		
		if( chars.contains(character) ){
			chars.remove(character);
		} else {
			chars.add(character);
		}
	}
	if( chars.isEmpty() ){
		throw new FistCharException();
	}		
	return chars.iterator().next();
}

```